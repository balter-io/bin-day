import pytest
from datetime import date


@pytest.fixture
def fake_date():
    return date(2021, 7, 1)
