from dotenv import load_dotenv
from datetime import date

load_dotenv()


class TestCollectionMethods:

    def test_date(self, fake_date):
        assert fake_date == date(2021, 7, 1)
