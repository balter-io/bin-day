import requests
import os
from dotenv import load_dotenv

load_dotenv()


class TestApiResponses:

    def test_day_api_response_status_equals_200(self):
        response = requests.get(os.getenv('day'))
        assert response.status_code == 200

    def test_day_response_type_equals_json(self):
        response = requests.get(os.getenv('day'))
        assert response.headers['Content-Type'] == 'application/json;charset=utf-8'

    def test_week_api_response_status_equals_200(self):
        response = requests.get(os.getenv('week'))
        assert response.status_code == 200

    def test_week_response_type_equals_json(self):
        response = requests.get(os.getenv('week'))
        assert response.headers['Content-Type'] == 'application/json;charset=utf-8'
