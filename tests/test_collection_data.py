import os
from waste_resources import requests as waste_requests
from dotenv import load_dotenv

load_dotenv()

waste_requests = waste_requests.WasteRequests()
day = waste_requests.get_day_request()
week = waste_requests.get_week_request()


class TestCollectionData:

    def test_resource_id(self):
        assert day['result']['resource_id'] == os.getenv('API_RESOURCE_ID')

    def test_record_id(self):
        assert day['result']['records'][0]['_id'] == int(os.getenv('PROPERTY_RECORD_ID'))

    def test_property_data(self):
        assert day['result']['records'][0]['HOUSE_NUMBER'] == os.getenv('HOUSE_NUMBER')
        assert day['result']['records'][0]['STREET_NAME'] == os.getenv('STREET_NAME')
        assert day['result']['records'][0]['SUBURB'] == os.getenv('SUBURB')
        assert day['result']['records'][0]['COLLECTION_DAY'] == os.getenv('COLLECTION_DAY')
        assert day['result']['records'][0]['ZONE'] == os.getenv('ZONE')

    def test_recycling_weeks(self):
        for record in week['result']['records']:
            if record['ZONE'] == day['result']['records'][0]['ZONE']:
                assert record['ZONE'].upper() == 'ZONE 2'
                assert (record['_id'] % 2) != 0
