import os
import requests
from dotenv import load_dotenv
load_dotenv()


class WasteRequests:

    def __init__(self):
        self.day_request = os.getenv('day')
        self.week_request = os.getenv('week')

    def get_day_request(self):
        return requests.get(self.day_request).json()

    def get_week_request(self):
        return requests.get(self.week_request).json()
